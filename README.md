# Expedia-Globant Challenge

## Challenge description
Develop a solution that solves the Business Requirement and make sure that any assumption is documented and that your code is production-ready.
Remember to include at least a Unit Test as a part of your solution.

## Assumptions
- The provided API is not working, so I decided to save data in an In-Memory Database (H2 in this case). I assume that the flight info dont change over time so with this in mind, saving it in a database is a good idea.
- If data changes over time, it would be a good idea caching that data and reload it after a given time, it depends on the problem.
- If an API is implemented to get the flights, it is mandatory to implement a CircuitBreaker to deal with communication problems.
- This api is only for read and query, not to create, update or delete.

## Features
The API has the following endpoint:
* GET /flights --> It returns all the flights in the system.
  It has two optionals query param: 
```
departureDatetime=2019-10-14T16:44:00Z
airline=UNITED AIRLINES
```

* Response example:

```
[
  {
    "id": 6,
    "from": "MIA",
    "to": "WAS",
    "airline": "JETBLUE AIRLINES",
    "number": "JA6161",
    "departureDatetime": "2019-10-14T13:44:00-03:00",
    "gate": "24"
  }
]
```


## To Improve
- Endpoint GET /flights is not paginated. This is mandatory for a production code, not this case.
- Endpoint has no security. This depends on the Security of the company, sometimes Microservices are behind for ex. behind an Api Gateway.
- There are only unit test code for Service Layer. Is necessary to do more unit test (in controller and repository for example)
- There are no Integration Test. Is necessary to create more at least to achieve 80% of coverage.


## How to Run locally

* java 11
* maven

### Dependencies

```
cd [root project]
mvn clean install
java -jar target/globant-0.0.1-SNAPSHOT.jar
```

It runs on localhost:8080

## How to Run with Docker

### Dependencies

* docker
* docker-compose


```
cd [root project]
docker-compose up -d
```
