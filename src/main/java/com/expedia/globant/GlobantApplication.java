package com.expedia.globant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class GlobantApplication {

	public static void main(String[] args) {
		SpringApplication.run(GlobantApplication.class, args);
	}

}
