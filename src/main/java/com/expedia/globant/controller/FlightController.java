package com.expedia.globant.controller;

import com.expedia.globant.model.Flight;
import com.expedia.globant.model.dto.FlightDto;
import com.expedia.globant.service.IFlightService;
import com.expedia.globant.service.impl.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/flights")
public class FlightController {

    @Autowired
    private IFlightService flightService;

    @GetMapping
    //Departure Date and/or  Airline Code.
    public ResponseEntity<List<FlightDto>> getAllFlights(
            @RequestParam(required = false, name = "departureDatetime") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) OffsetDateTime dateTime,
            @RequestParam(required = false) String airline
    ) {
        return ResponseEntity.ok(
                this.flightService.findAllByDepartureDateAndOrAirlineCode(dateTime, airline).stream().map(FlightDto::from).collect(Collectors.toList())
        );
    }
}
