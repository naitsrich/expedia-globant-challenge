package com.expedia.globant.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Flight {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(name = "flight_from")
    private String from;
    @Column(name = "flight_to")
    private String to;
    private String airline;
    private String number;
    private OffsetDateTime departureDatetime;
    private String gate;
}

/*
[
        {
        "from": "NYC",
        "to": "SEA",
        "airline": "United Airlines",
        "flight_number": "1234",
        "departure_datetime":"2019-10-15T20:00:00.000Z",
        "gate": "24"
        }
        ]

 */