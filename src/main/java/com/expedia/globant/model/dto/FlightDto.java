package com.expedia.globant.model.dto;

import com.expedia.globant.model.Flight;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.OffsetDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FlightDto {
    private Long id;

    private String from;
    private String to;
    private String airline;
    private String number;
    private OffsetDateTime departureDatetime;
    private String gate;

    public static FlightDto from(Flight flight) {
        return FlightDto.builder()
                .id(flight.getId())
                .from(flight.getFrom())
                .to(flight.getTo())
                .airline(flight.getAirline())
                .number(flight.getNumber())
                .departureDatetime(flight.getDepartureDatetime())
                .gate(flight.getGate())
                .build();
    }
}
