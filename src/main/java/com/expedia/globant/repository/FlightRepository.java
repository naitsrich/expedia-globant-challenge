package com.expedia.globant.repository;

import com.expedia.globant.model.Flight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.OffsetDateTime;
import java.util.List;

@Repository
public interface FlightRepository extends JpaRepository<Flight, Long> {

    @Query("FROM Flight f WHERE (f.departureDatetime = :departureDate OR :departureDate IS NULL) AND (f.airline = :airline OR :airline IS NULL)")
    List<Flight> findAllByDepartureDateAndOrAirlineCode(@Param("departureDate")OffsetDateTime departureDate, @Param("airline") String airline);
}
