package com.expedia.globant.service;

import com.expedia.globant.model.Flight;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.List;

public interface IFlightService {
    List<Flight> findAll();

    List<Flight> findAllByDepartureDateAndOrAirlineCode(OffsetDateTime offsetDateTime, String airlineCode);
}
