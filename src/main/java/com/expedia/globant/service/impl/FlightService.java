package com.expedia.globant.service.impl;

import com.expedia.globant.model.Flight;
import com.expedia.globant.repository.FlightRepository;
import com.expedia.globant.service.IFlightService;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.List;

@Service
public class FlightService implements IFlightService {
    FlightRepository flightRepository;

    public FlightService(FlightRepository flightRepository) {
        this.flightRepository = flightRepository;
    }
    @Override
    public List<Flight> findAll() {
        return this.flightRepository.findAll();
    }

    @Override
    public List<Flight> findAllByDepartureDateAndOrAirlineCode(OffsetDateTime departureDatetime, String airline) {
        return this.flightRepository.findAllByDepartureDateAndOrAirlineCode(departureDatetime, airline);
    }
}
