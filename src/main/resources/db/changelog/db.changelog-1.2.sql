INSERT INTO flight (id, flight_from, flight_to, airline, number, departure_datetime, gate) VALUES(1, 'NYC', 'SEA', 'UNITED AIRLINES', '1234','2019-10-15T20:00:00.000Z', '24');
INSERT INTO flight (id, flight_from, flight_to, airline, number, departure_datetime, gate) VALUES(2, 'WAS', 'MIA', 'UNITED AIRLINES', 'UA123','2019-10-15T21:00:00.000Z', 'A2');
INSERT INTO flight (id, flight_from, flight_to, airline, number, departure_datetime, gate) VALUES(3, 'NYC', 'MIA', 'AMERICAN AIRLINES', 'AA966','2019-10-15T13:00:00.000Z', '9');
INSERT INTO flight (id, flight_from, flight_to, airline, number, departure_datetime, gate) VALUES(4, 'NYC', 'MIA', 'SPIRIT AIRLINES', 'SA522','2019-10-15T18:30:00.000Z', '2');
INSERT INTO flight (id, flight_from, flight_to, airline, number, departure_datetime, gate) VALUES(5, 'MIA', 'NYC', 'FRONTIER AIRLINES', 'FA7283','2019-10-16T09:35:00.000Z', 'B77');
INSERT INTO flight (id, flight_from, flight_to, airline, number, departure_datetime, gate) VALUES(6, 'MIA', 'WAS', 'JETBLUE AIRLINES', 'JA6161','2019-10-14T16:44:00.000Z', '24');
