package com.expedia.globant;

import com.expedia.globant.model.Flight;
import com.expedia.globant.repository.FlightRepository;
import com.expedia.globant.service.impl.FlightService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.OffsetDateTime;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FlightServiceUnitTest {

    @Mock
    private FlightRepository flightRepository;

    @InjectMocks
    private FlightService flightService;

    @Test
    public void givenNoFilters_whenIsFindAllByDepartureDateAndOrAirlineCode_thenAllFlightsAreReturned() throws JsonProcessingException {
        final String allFlightsJson = "[\n" +
                "    {\n" +
                "        \"id\": 1,\n" +
                "        \"from\": \"NYC\",\n" +
                "        \"to\": \"SEA\",\n" +
                "        \"airline\": \"UNITED AIRLINES\",\n" +
                "        \"number\": \"1234\",\n" +
                "        \"departureDatetime\": \"2019-10-15T17:00:00-03:00\",\n" +
                "        \"gate\": \"24\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 2,\n" +
                "        \"from\": \"WAS\",\n" +
                "        \"to\": \"MIA\",\n" +
                "        \"airline\": \"UNITED AIRLINES\",\n" +
                "        \"number\": \"UA123\",\n" +
                "        \"departureDatetime\": \"2019-10-15T18:00:00-03:00\",\n" +
                "        \"gate\": \"A2\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 3,\n" +
                "        \"from\": \"NYC\",\n" +
                "        \"to\": \"MIA\",\n" +
                "        \"airline\": \"AMERICAN AIRLINES\",\n" +
                "        \"number\": \"AA966\",\n" +
                "        \"departureDatetime\": \"2019-10-15T10:00:00-03:00\",\n" +
                "        \"gate\": \"9\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 4,\n" +
                "        \"from\": \"NYC\",\n" +
                "        \"to\": \"MIA\",\n" +
                "        \"airline\": \"SPIRIT AIRLINES\",\n" +
                "        \"number\": \"SA522\",\n" +
                "        \"departureDatetime\": \"2019-10-15T15:30:00-03:00\",\n" +
                "        \"gate\": \"2\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 5,\n" +
                "        \"from\": \"MIA\",\n" +
                "        \"to\": \"NYC\",\n" +
                "        \"airline\": \"FRONTIER AIRLINES\",\n" +
                "        \"number\": \"FA7283\",\n" +
                "        \"departureDatetime\": \"2019-10-16T06:35:00-03:00\",\n" +
                "        \"gate\": \"B77\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 6,\n" +
                "        \"from\": \"MIA\",\n" +
                "        \"to\": \"WAS\",\n" +
                "        \"airline\": \"JETBLUE AIRLINES\",\n" +
                "        \"number\": \"JA6161\",\n" +
                "        \"departureDatetime\": \"2019-10-14T13:44:00-03:00\",\n" +
                "        \"gate\": \"24\"\n" +
                "    }\n" +
                "]";


        ObjectMapper objectMapper1 = new ObjectMapper();
        objectMapper1.registerModule(new JavaTimeModule());

        List<Flight> flightsMock = objectMapper1.readValue(allFlightsJson, new TypeReference<List<Flight>>(){});

        when(this.flightRepository.findAllByDepartureDateAndOrAirlineCode(null, null)).thenReturn(flightsMock);

        List<Flight> flights = this.flightService.findAllByDepartureDateAndOrAirlineCode(null,null);

        Assertions.assertEquals(flightsMock.size(), flights.size());
    }

    @Test
    public void givenDepartureDateFilter_whenIsFindAllByDepartureDateAndOrAirlineCode_thenOneFlightWithDepartureDateAreReturned() throws JsonProcessingException {
        final String allFlightsJson = "[\n" +
                "    {\n" +
                "        \"id\": 6,\n" +
                "        \"from\": \"MIA\",\n" +
                "        \"to\": \"WAS\",\n" +
                "        \"airline\": \"JETBLUE AIRLINES\",\n" +
                "        \"number\": \"JA6161\",\n" +
                "        \"departureDatetime\": \"2019-10-14T13:44:00-03:00\",\n" +
                "        \"gate\": \"24\"\n" +
                "    }\n" +
                "]";


        ObjectMapper objectMapper1 = new ObjectMapper();
        objectMapper1.registerModule(new JavaTimeModule());

        List<Flight> flightsMock = objectMapper1.readValue(allFlightsJson, new TypeReference<List<Flight>>(){});

        OffsetDateTime departureDate = OffsetDateTime.parse("2019-10-14T16:44:00Z");

        when(this.flightRepository.findAllByDepartureDateAndOrAirlineCode(departureDate, null)).thenReturn(flightsMock);


        List<Flight> flights = this.flightService.findAllByDepartureDateAndOrAirlineCode(departureDate,null);

        Assertions.assertEquals(flightsMock.size(), flights.size());
    }

    @Test
    public void givenDepartureDateAndAirlineFilter_whenIsFindAllByDepartureDateAndOrAirlineCode_thenOneFlightWithDepartureDateAndAirlineAreReturned() throws JsonProcessingException {
        final String allFlightsJson = "[\n" +
                "    {\n" +
                "        \"id\": 6,\n" +
                "        \"from\": \"MIA\",\n" +
                "        \"to\": \"WAS\",\n" +
                "        \"airline\": \"JETBLUE AIRLINES\",\n" +
                "        \"number\": \"JA6161\",\n" +
                "        \"departureDatetime\": \"2019-10-14T13:44:00-03:00\",\n" +
                "        \"gate\": \"24\"\n" +
                "    }\n" +
                "]";


        ObjectMapper objectMapper1 = new ObjectMapper();
        objectMapper1.registerModule(new JavaTimeModule());

        List<Flight> flightsMock = objectMapper1.readValue(allFlightsJson, new TypeReference<List<Flight>>(){});

        OffsetDateTime departureDate = OffsetDateTime.parse("2019-10-14T16:44:00Z");
        String airline = "JETBLUE AIRLINES";
        when(this.flightRepository.findAllByDepartureDateAndOrAirlineCode(departureDate, airline)).thenReturn(flightsMock);


        List<Flight> flights = this.flightService.findAllByDepartureDateAndOrAirlineCode(departureDate,airline);

        Assertions.assertEquals(flightsMock.size(), flights.size());
    }

    @Test
    public void givenAirlineFilter_whenIsFindAllByDepartureDateAndOrAirlineCode_thenOneFlightWithAirlineAreReturned() throws JsonProcessingException {
        final String allFlightsJson = "[\n" +
                "    {\n" +
                "        \"id\": 6,\n" +
                "        \"from\": \"MIA\",\n" +
                "        \"to\": \"WAS\",\n" +
                "        \"airline\": \"JETBLUE AIRLINES\",\n" +
                "        \"number\": \"JA6161\",\n" +
                "        \"departureDatetime\": \"2019-10-14T13:44:00-03:00\",\n" +
                "        \"gate\": \"24\"\n" +
                "    }\n" +
                "]";


        ObjectMapper objectMapper1 = new ObjectMapper();
        objectMapper1.registerModule(new JavaTimeModule());

        List<Flight> flightsMock = objectMapper1.readValue(allFlightsJson, new TypeReference<List<Flight>>(){});

        OffsetDateTime departureDate = null;
        String airline = "JETBLUE AIRLINES";
        when(this.flightRepository.findAllByDepartureDateAndOrAirlineCode(departureDate, airline)).thenReturn(flightsMock);

        List<Flight> flights = this.flightService.findAllByDepartureDateAndOrAirlineCode(departureDate,airline);

        Assertions.assertEquals(flightsMock.size(), flights.size());
    }
}
